import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt


def model(c, t):
    kf = 5
    kr = 0.1
    k1 = 1
    k2 = 0.55

    ca = c[0]
    cb = c[1]
    cd = c[2]

    da = -kf*ca + kr*cb + k2*cd
    dc = kf*ca - kr*cb - k1*cb
    dd = -k2*cd + k1*cb

    return da,dc, dd


t = np.linespace(0, 10, 1001)
c0 = [1, 0, 0]

c = odeint(model, c0, 1)

ca = c[t,0]


plt.show()
