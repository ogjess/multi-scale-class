import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt


def model(y,t):
    y1 = y[0]
    y2 = y[1]
    dy1dt = y2
    dy2dt = -0.5 * y1 - y2

    return dy1dt, dy2dt


y0 = [0, 1]

t = np.linspace(0, 10, 1001)

y = odeint(model, y0, t)

y1 = y[:, 0]
y2 = y[:, 1]

plt.plot(t, y1, c='k')
plt.plot(t, y2, c='r')

plt.show()

